from faker import Faker
import endpoints
import random
import requests

fake = Faker()


class City:
    def __init__(self):
        self.name = fake.uuid4()
        self.population = random.randint(1, 10000000)

    def create(self):
        body = {"name": self.name,
                "population": self.population}
        create_city_response = requests.post(endpoints.city_create, json=body)
        assert create_city_response.status_code == 201
        return create_city_response.json()['id']


