from faker import Faker
import endpoints
import requests
import pytest
from cities import City

fake = Faker()


@pytest.fixture
def city():
    newCity = City()
    return newCity.create()



def test_cities_list(city):
    response_cities_list = requests.get(endpoints.cities_list)
    assert response_cities_list.status_code == 200

def test_update(city):
    body = {
    "name": "My City",
    "population": 123
    }
    response_city_update = requests.put(endpoints.city_data_update + "/" + str(city), json=body)
    assert response_city_update.status_code == 200

def test_delete(city):
    response_test_delete = requests.delete(endpoints.city_delete + "/" + str(city))
    assert response_test_delete.status_code == 204

def test_get_city_data(city):
    response_city_data = requests.get(endpoints.city_data + "/" + str(city))
    assert response_city_data.status_code == 200
    new_city = response_city_data.json()
    print(new_city)

def test_city_create():
    body = {
        "name": "My City",
        "population": 123
    }
    response_city_create = requests.post(endpoints.city_create, json=body)
    assert response_city_create.status_code == 201

